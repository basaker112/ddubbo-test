package com.lyx.dubbotest.bootserviceprovider;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
// 开启dubbo
@EnableDubbo
public class BootServiceProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootServiceProviderApplication.class, args);
	}

}
