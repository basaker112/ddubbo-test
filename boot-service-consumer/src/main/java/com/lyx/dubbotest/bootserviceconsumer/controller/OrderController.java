package com.lyx.dubbotest.bootserviceconsumer.controller;

import com.lyx.dubbotest.bean.UserAddress;
import com.lyx.dubbotest.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

/**
 * @author LiuYunXin
 * @create 2022-01-06-16:06
 */
@Controller
public class OrderController {

    @Autowired
    OrderService orderService;

    @ResponseBody
    @RequestMapping("/initOrder")
    public List<UserAddress> initOrder(@RequestParam("id") String id) throws IOException {
        return orderService.initOrder(id);
    }
}
