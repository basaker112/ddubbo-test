package com.lyx.dubbotest.service;


import com.lyx.dubbotest.bean.UserAddress;

import java.io.IOException;
import java.util.List;

public interface OrderService {
	
	/**
	 * 初始化订单
	 * @param userId
	 */
	public List<UserAddress> initOrder(String userId) throws IOException;

}
