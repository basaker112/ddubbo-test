package com.lyx.dubbotest.service.impl;

import com.alibaba.dubbo.common.json.JSON;
import com.lyx.dubbotest.bean.UserAddress;
import com.lyx.dubbotest.service.OrderService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author LiuYunXin
 * @create 2022-01-06-15:28
 */
public class MainApplication {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext ioc = new ClassPathXmlApplicationContext("consumer.xml");
        OrderService bean = ioc.getBean(OrderService.class);
        List<UserAddress> userAddresses = bean.initOrder("111");
        System.out.println(JSON.json(userAddresses));
    }
}
